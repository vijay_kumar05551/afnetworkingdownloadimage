//
//  AppDelegate.h
//  AFNetworking
//
//  Created by clicklabs70 on 11/18/15.
//  Copyright © 2015 click-labs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


//
//  ViewController.m
//  AFNetworking
//
//  Created by clicklabs70 on 11/18/15.
//  Copyright © 2015 click-labs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //[self getApi];
    [self postApi];
          

}


-(void)postApi{

    
    NSDictionary *params = @{@"key":@"293a64eef15b452047a0315ecc6eff2a9bbd4a9c"};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager POST:[NSString stringWithFormat:@"https://www.hostedredmine.com/projects/taskconnector-test/issues.json"]
       parameters:params
          success:^(AFHTTPRequestOperation *operation, id json) {
              if (json) {
                  NSLog(@"%@",json);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error.description);
              NSLog(@"ERROR: %@ \n\n 1. ",error.localizedDescription);
              
              
              
          }];
}

-(void) getApi{
   
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"http://api.openweathermap.org/data/2.5/weather?q=Delhi,India&appid=2de143494c0b295cca9337e1e96b00e0"]
       parameters:nil
          success:^(AFHTTPRequestOperation *operation, id json) {
              if (json) {
                  NSLog(@"%@",json);
              }
          }
          failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              NSLog(@"Error: %@", error.description);
              NSLog(@"ERROR: %@ \n\n 1. ",error.localizedDescription);
              
              
              
          }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
